# == Schema Information
#
# Table name: users
#
#  id         :bigint           not null, primary key
#  address    :string
#  name       :string
#  password   :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
FactoryBot.define do
  factory :user do
    name { "MyString" }
    password { "MyString" }
    address { "MyString" }
  end
end
