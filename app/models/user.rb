# == Schema Information
#
# Table name: users
#
#  id         :bigint           not null, primary key
#  address    :string
#  name       :string
#  password   :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class User < ApplicationRecord
end
